import React from 'react'
import './App.css'


class App extends React.Component {
  render(){
    return(
          <div className="App">
            <h3> This is the beginning of the exercise.</h3>
            <p>Feel free to add or edit any files in the project.</p>
            <p>Have fun!</p>
          </div>
      )
  }
}

export default App