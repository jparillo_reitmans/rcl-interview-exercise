export const accessories = [
    {
        id: "437626",
        title: "Multi Beads Short Necklace",
        category: "Accessories",
        price: 18.90,
        color: "Multicolors",
        image: "https://www.reitmans.com/on/demandware.static/-/Sites-Reitmans-catalog/default/dwf91d35fd/images/xlarge/reitmans_437626_966_0.jpg"
    },
    {
        id: "438016",
        title: "Braided Chains Necklace",
        category: "Accessories",
        price: 18.90,
        color: "Nostalgia Rose",
        image: "https://www.reitmans.com/on/demandware.static/-/Sites-Reitmans-catalog/default/dwd119002c/images/xlarge/reitmans_438016_666_0.jpg"
    },
    {
        id: "438376",
        title: "Floral & Leaves Print Oblong Scarf",
        category: "Accessories",
        price: 19.90,
        color: "Sepia Rose",
        image: "https://www.reitmans.com/on/demandware.static/-/Sites-Reitmans-catalog/default/dw51367913/images/xlarge/reitmans_438376_684_0.jpg"
    },
    {
        id: "438130",
        title: "Wood & Metal Drop Earrings",
        category: "Accessories",
        price: 12.90,
        color: "Honey Gold",
        image: "https://www.reitmans.com/on/demandware.static/-/Sites-Reitmans-catalog/default/dw87e64e3c/images/xlarge/reitmans_438130_728_0.jpg"
    },
    {
        id: "437052",
        title: "Flapover Crossbody Bag",
        category: "Accessories",
        price: 44.90,
        color: "Black",
        image: "https://www.reitmans.com/on/demandware.static/-/Sites-Reitmans-catalog/default/dw42318b88/images/xlarge/reitmans_437052_1_0.jpg"
    },
    {
        id: "436859",
        title: "Crinkled Floral Print Scarf",
        category: "Accessories",
        price: 19.90,
        color: "Nostalgia Rose",
        image: "https://www.reitmans.com/on/demandware.static/-/Sites-Reitmans-catalog/default/dw2aae36b7/images/xlarge/reitmans_436859_666_0.jpg"
    },
    {
        id: "434920",
        title: "3-Pack Stripe Ankle Socks",
        category: "Accessories",
        price: 9.03,
        color: "Multicolors",
        image: "https://www.reitmans.com/on/demandware.static/-/Sites-Reitmans-catalog/default/dw811659c6/images/xlarge/reitmans_434920_966_0.jpg"
    },
    {
        id: "43708",
        title: "2-Tone Ladylike Bag",
        category: "Accessories",
        price: 54.90,
        color: "Taupe",
        image: "https://www.reitmans.com/on/demandware.static/-/Sites-Reitmans-catalog/default/dwfbca89cd/images/xlarge/reitmans_437081_294_0.jpg"
    }
]