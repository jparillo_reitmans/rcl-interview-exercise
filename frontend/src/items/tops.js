export const tops = [
    {
        id: "435392",
        title: "Short Sleeve Mix Media Tee with Polo Collar",
        category: "Tops",
        price: 25.83,
        color: "Marshmallow",
        image: "https://www.reitmans.com/on/demandware.static/-/Sites-Reitmans-catalog/default/dwba615bc4/images/xlarge/reitmans_435392_101_0.jpg"
    },
    {
        id: "434336",
        title: "Cotton Blend Short Sleeve Tee R Essentials",
        category: "Tops",
        price: 19.50,
        color: "Bright White",
        image: "https://www.reitmans.com/on/demandware.static/-/Sites-Reitmans-catalog/default/dwcd08002a/images/xlarge/reitmans_434336_100_0.jpg"
    },
    {
        id: "433295",
        title: "Short Sleeve Striped Crew Neck Blouse",
        category: "Tops",
        price: 30.03,
        color: "Black",
        image: "https://www.reitmans.com/on/demandware.static/-/Sites-Reitmans-catalog/default/dwbc9362a3/images/xlarge/reitmans_433295_1_0.jpg"
    },
    {
        id: "432875",
        title: "Printed Hoodie with Drawstring",
        category: "Tops",
        price: 38.43,
        color: "Black",
        image: "https://www.reitmans.com/on/demandware.static/-/Sites-Reitmans-catalog/default/dw84cb9db7/images/xlarge/reitmans_432875_1_0.jpg"
    },
    {
        id: "436270",
        title: "Short Sleeve Notch Collar Blouse",
        category: "Tops",
        price: 31.43,
        color: "Black",
        image: "https://www.reitmans.com/on/demandware.static/-/Sites-Reitmans-catalog/default/dwbf957b3f/images/xlarge/reitmans_436270_1_0.jpg"
    },
    {
        id: "440235",
        title: "Long Sleeve Tee with Shirring at Shoulders",
        category: "Tops",
        price: 25.83,
        color: "Fig",
        image: "https://www.reitmans.com/on/demandware.static/-/Sites-Reitmans-catalog/default/dwf45531a0/images/xlarge/reitmans_440235_603_0.jpg"
    },
    {
        id: "432862",
        title: "Printed Short Sleeve Crew Neck Tee",
        category: "Tops",
        price: 25.83,
        color: "Grey",
        image: "https://www.reitmans.com/on/demandware.static/-/Sites-Reitmans-catalog/default/dw9855583d/images/xlarge/reitmans_432862_31_0.jpg"
    },
    {
        id: "432458",
        title: "Buttoned Duster Cardigan with Pockets",
        category: "Tops",
        price: 45.43,
        color: "Khaki",
        image: "https://www.reitmans.com/on/demandware.static/-/Sites-Reitmans-catalog/default/dw7fa91206/images/xlarge/reitmans_432458_350_0.jpg"
    },
]