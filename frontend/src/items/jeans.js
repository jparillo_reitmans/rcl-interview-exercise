export const jeans = [
    {
        id: "437424",
        title: "5-Button High Rise Skinny Jeans",
        category: "Jeans",
        price: 59.90,
        color: "Rinse Denim",
        image: "https://www.reitmans.com/on/demandware.static/-/Sites-Reitmans-catalog/default/dw1b169e4b/images/xlarge/reitmans_437424_497_0.jpg"
    },
    {
        id: "430272",
        title: "Skinny Jeans The Super High Rise",
        category: "Jeans",
        price: 59.90,
        color: "Medium Denim",
        image: "https://www.reitmans.com/on/demandware.static/-/Sites-Reitmans-catalog/default/dwa0de89f7/images/xlarge/reitmans_430272_480_0.jpg"
    },
    {
        id: "433140",
        title: "High Rise Skinny Jeans The Signature Soft",
        category: "Jeans",
        price: 54.90,
        color: "Dark Denim",
        image: "https://www.reitmans.com/on/demandware.static/-/Sites-Reitmans-catalog/default/dwa017abef/images/xlarge/reitmans_433140_498_0.jpg"
    },
    {
        id: "435706",
        title: "Black Skinny Jeans with Zip Details",
        category: "Jeans",
        price: 59.90,
        color: "Black",
        image: "https://www.reitmans.com/on/demandware.static/-/Sites-Reitmans-catalog/default/dw871851d9/images/xlarge/reitmans_435706_1_0.jpg"
    },
    {
        id: "430273",
        title: "Skinny Jeans The Super High Rise",
        category: "Jeans",
        price: 59.90,
        color: "Dark Denim",
        image: "https://www.reitmans.com/on/demandware.static/-/Sites-Reitmans-catalog/default/dwb2403c18/images/xlarge/reitmans_430273_498_0.jpg"
    },
    {
        id: "433123",
        title: "High Waist Skinny Black Jeans The Signature Soft",
        category: "Jeans",
        price: 54.90,
        color: "Black",
        image: "https://www.reitmans.com/on/demandware.static/-/Sites-Reitmans-catalog/default/dwfe1ea128/images/xlarge/reitmans_433123_1_0.jpg"
    },
    {
        id: "408175",
        title: "The Insider Skinny Jeans",
        category: "Jeans",
        price: 59.90,
        color: "Dark Denim",
        image: "https://www.reitmans.com/on/demandware.static/-/Sites-Reitmans-catalog/default/dw19a785e8/images/xlarge/reitmans_408175_498_0.jpg"
    },
    {
        id: "408181",
        title: "Skinny Jeans The Sculpting",
        category: "Jeans",
        price: 64.90,
        color: "Dark Denim",
        image: "https://www.reitmans.com/on/demandware.static/-/Sites-Reitmans-catalog/default/dwb6abfdcb/images/xlarge/reitmans_408181_498_0.jpg"
    }
]