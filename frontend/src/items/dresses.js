export const dresses = [
    {
        id: "425011",
        title: "Short Sleeve Shift Dress with Faux Leather Inserts",
        category: "Dresses",
        price: 64.90,
        color: "Black",
        image: "https://www.reitmans.com/on/demandware.static/-/Sites-Reitmans-catalog/default/dw12484edc/images/xlarge/reitmans_425011_1_0.jpg"
    },
    {
        id: "430686",
        title: "Fit & Flare Printed Sleeveless Dress",
        category: "Dresses",
        price: 34.93,
        color: "Grey Violet",
        image: "https://www.reitmans.com/on/demandware.static/-/Sites-Reitmans-catalog/default/dw71043ab7/images/xlarge/reitmans_430686_57_0.jpg"
    },
    {
        id: "429971",
        title: "Short Sleeve Shift Dress with Braided Cord",
        category: "Dresses",
        price: 34.93,
        color: "Black",
        image: "https://www.reitmans.com/on/demandware.static/-/Sites-Reitmans-catalog/default/dw6e567499/images/xlarge/reitmans_429971_1_0.jpg"
    },
    {
        id: "429985",
        title: "Halter Neck Printed Maxi Dress",
        category: "Dresses",
        price: 41.93,
        color: "Verona Blue",
        image: "https://www.reitmans.com/on/demandware.static/-/Sites-Reitmans-catalog/default/dw2030800f/images/xlarge/reitmans_429985_415_0.jpg"
    },
    {
        id: "431580",
        title: "Sleeveless Printed Swing Dress with Smocking",
        category: "Dresses",
        price: 34.93,
        color: "Verona Blue",
        image: "https://www.reitmans.com/on/demandware.static/-/Sites-Reitmans-catalog/default/dwfe3817f1/images/xlarge/reitmans_431580_415_0.jpg"
    },
    {
        id: "429986",
        title: "Shift Printed Sleeveless Midi Dress",
        category: "Dresses",
        price: 34.93,
        color: "Verona Blue",
        image: "https://www.reitmans.com/on/demandware.static/-/Sites-Reitmans-catalog/default/dw0a6219fe/images/xlarge/reitmans_429986_415_0.jpg"
    },
    {
        id: "429982",
        title: "Sleeveless Maxi Dress",
        category: "Dresses",
        price: 38.43,
        color: "Valiant Poppy",
        image: "https://www.reitmans.com/on/demandware.static/-/Sites-Reitmans-catalog/default/dw869597b8/images/xlarge/reitmans_429982_610_0.jpg"
    },
    {
        id: "429968",
        title: "Sleeveless Printed Swing Dress with Handkerchief Hem",
        category: "Dresses",
        price: 31.43,
        color: "Deep Sea Coral",
        image: "https://www.reitmans.com/on/demandware.static/-/Sites-Reitmans-catalog/default/dw32f9daa6/images/xlarge/reitmans_429968_669_0.jpg"
    }
]