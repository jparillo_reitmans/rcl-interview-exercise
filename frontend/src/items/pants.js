export const pants = [
    {
        id: "426258",
        title: "Black Straight Pull On Pants The Modern Stretch",
        category: "Pants",
        price: 34.90,
        color: "Black",
        image: "https://www.reitmans.com/on/demandware.static/-/Sites-Reitmans-catalog/default/dwad46dcb1/images/xlarge/reitmans_426258_1_0.jpg"
    },
    {
        id: "424552",
        title: "Straight Leg Pull On Grey Pants The Iconic",
        category: "Pants",
        price: 49.90,
        color: "Black",
        image: "https://www.reitmans.com/on/demandware.static/-/Sites-Reitmans-catalog/default/dw36e856e9/images/xlarge/reitmans_424552_63_0.jpg"
    },
    {
        id: "426282",
        title: "Straight Pull On Pants The Iconic",
        category: "Pants",
        price: 49.90,
        color: "Black",
        image: "https://www.reitmans.com/on/demandware.static/-/Sites-Reitmans-catalog/default/dwfdfeb144/images/xlarge/reitmans_426282_1_0.jpg"
    },
    {
        id: "428088",
        title: "The Iconic Jacquard Straight Pull On Pants",
        category: "Pants",
        price: 49.90,
        color: "Black",
        image: "https://www.reitmans.com/on/demandware.static/-/Sites-Reitmans-catalog/default/dw6e9fbb81/images/xlarge/reitmans_428088_1_0.jpg"
    },
    {
        id: "424723",
        title: "The New Classic Straight Pants with Side Metal Buckles",
        category: "Pants",
        price: 34.90,
        color: "Smoke Pearl",
        image: "https://www.reitmans.com/on/demandware.static/-/Sites-Reitmans-catalog/default/dw937f3c05/images/xlarge/reitmans_424723_63_0.jpg"
    },
    {
        id: "431554",
        title: "Straight Pull On Pants The Iconic",
        category: "Pants",
        price: 49.90,
        color: "Pomagranate",
        image: "https://www.reitmans.com/on/demandware.static/-/Sites-Reitmans-catalog/default/dw4c089eb0/images/xlarge/reitmans_431554_605_0.jpg"
    },
    {
        id: "431789",
        title: "Plaid Straight Pants with Sash",
        category: "Pants",
        price: 54.90,
        color: "Heather Iron",
        image: "https://www.reitmans.com/on/demandware.static/-/Sites-Reitmans-catalog/default/dw9509ef22/images/xlarge/reitmans_431789_62_0.jpg"
    },
    {
        id: "428017",
        title: "The Iconic Jacquard Straight Pull On Pants",
        category: "Pants",
        price: 49.90,
        color: "Verona Blue",
        image: "https://www.reitmans.com/on/demandware.static/-/Sites-Reitmans-catalog/default/dw5d98d2c9/images/xlarge/reitmans_428017_415_1.jpg"
    }
]