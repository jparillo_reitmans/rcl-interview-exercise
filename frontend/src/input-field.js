import React from 'react'

const InputField = ({input, label, type, meta: { touched, error, warning }}) => (
    <div>
      <input {...input} placeholder={label} type={type} />
      {touched && ((error && <div className="error-message">{error}</div>) || (warning && <div>{warning}</div>))}
    </div>
)

export default InputField