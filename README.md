# Reitmans (Canada) Limited Interview Exercise
  
## Requirements
  
Create a simple but __impressive__ (looks good, works well, etc.) Product Listing Page that can do the following: 

 1. Retrieve products from a REST API
 2. Display products in a React application
 3. Manage and store the product data with Redux
 4. Has UI mechanisms for filtering and sorting Products
 5. Edit your version of the `README.md` file to explain to us what things you did, your effort was focused on, etc.
   
__This is your chance to amaze us with your talent!__
  
*Read over the `Bonus` objectives and consider tackling those items as well*

## Bonus (Highly Encouraged)
  
 1. Use a relational database to store and retrieve the data (SQLite, MariaDB, Postgres, MySql, etc.)
 2. Add API endpoint to create, delete and update product data
 3. Save your application to a persistence storage.
  
## Stretch Bonus

 1. Create a Product Details Page and display the product name, image, category, price, color, etc.
 2. Add interactive functionality such as color and sizing options, add to cart button, etc.
  
## Getting Started
  
 1. clone the repository on your computer
 2. cd in frontend
 3. In the terminal, write npm install
 4. then write npm start
 5. A browser tab will open at https://localhost:3000
 6. You should see the following message displayed "This is the beginning of the excercise. Feel free to add or edit any files in the project. Have fun!"
  
The static json data is located in /frontend/src/items/

## Getting it Done
  
* You are free to use whatever libraries that you want. Be prepared to defend your decisions. Here is a list of some packages that may help you to complete the exercise:
	* React-router-dom
	* Redux
	* React-redux
	* Redux-persist
	* Redux-devtools-extension
* There is no time limit. Use as little or as much time as is necessary to showcase your abilities.
* You should fork or clone our repository into your own repository.
* Send us the link when you are done with the exercise.
* We will schedule a second interview either in person or online to go over your code.